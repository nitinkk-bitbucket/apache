#!/bin/bash

#USER=portal
#GROUP=portal

EXT_MOUNTS_HOME=/opt/externals
LICENSE_DIR=${EXT_MOUNTS_HOME}/license
PROPERTY_DIR=${EXT_MOUNTS_HOME}/properties
FILES_DIR=${EXT_MOUNTS_HOME}/files
ARTIFACTS_DIR=${EXT_MOUNTS_HOME}/artifacts

# Create missing directories if not present :
[ ! -d "$LICENSE_DIR" ] && mkdir -p $LICENSE_DIR
[ ! -d "$PROPERTY_DIR" ] && mkdir -p $PROPERTY_DIR
[ ! -d "$ARTIFACTS_DIR" ] && mkdir -p $ARTIFACTS_DIR


echo "starting apache"

/usr/bin/supervisord -c /etc/supervisor/supervisord.conf

exec "$@"
